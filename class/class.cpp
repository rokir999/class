﻿#include <iostream>
#include <cmath>

class Vector
{
private:
    double a = 1;
    double b = 2;
    double c = 3;
public:
    double GetA()
    {
        return a;
    }
    double GetB()
    {
        return b;
    }
    double GetC()
    {
        return c;
    }
};

int main()
{
    Vector temp;
    temp.GetA();
    temp.GetB();
    temp.GetC();
    std::cout << temp.GetA() <<" " << temp.GetB() <<" " << temp.GetC() <<"\n" ;
    std::cout << sqrt(pow(temp.GetA(), 2) + pow(temp.GetB(), 2) + pow(temp.GetC(), 2));
}

